"use client";

import { useSearchParams } from "next/navigation";
import { Button } from "@/components/ui/button";
import Link from "next/link";

export default function KatakanaGameoverPage({ params }: {
    params: {
        level: string;
    };
}) {
    const searchParams = useSearchParams();
    const score = searchParams.get("score");

    return (
        <main className="flex flex-col gap-4 items-center justify-center h-full w-full">
            <h1 className="text-4xl font-bold">Game Over</h1>
            <h2 className="text-xl font-bold">Level: {params.level}</h2>
            <h2 className="text-xl font-bold">Your Score: {score}</h2>
            <Button asChild>
                <Link href={`/katakana/${params.level}`}>
                    Play again!
                </Link>
            </Button>
        </main>
    );
}
