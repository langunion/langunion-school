"use client";

import { useState, useEffect, useMemo, ReactNode } from "react";
import { toKatakana } from "wanakana";
import { animate } from "framer-motion";
import { Card, CardContent } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import katakanaWords from "../Katakana.Words";
import { useRouter } from "next/navigation"; 
import clsx from "clsx";

export default function KatakanaLevelPage({ params }: {
    params: {
        level: string;
    }
}) {
    const router = useRouter();

    const [count, setCount] = useState<number>(0);
    const [katakana, setKatakana] = useState<string>("");
    const [input, setInput] = useState<string>("");
    const words = useMemo<string[]>(() => katakanaWords[params.level as "wa"], [params.level]);
    const score = useMemo<number>(() => count * 100, [count]);
    const ttl = useMemo<number>(() => {
        const start = 30;
        const ttl = count * Math.exp(-1 * count) + start;
        console.log({ ttl });
        return ttl;
    }, [count]);
    const preview = useMemo<ReactNode[]>(() => {
        const katakanaChars: string[] = katakana.split("");
        const inputChars: string[] = toKatakana(input).split("");

        return katakanaChars.map((char: string, index: number) => {
            let isCorrect: boolean = char === inputChars[index];
            return (
                <span
                    key={index}
                    className={clsx(
                        "font-bold text-4xl",
                        isCorrect && "text-lime-500"
                    )}
                >
                    {char}
                </span>
            );
        });
    }, [input, katakana]);

    useEffect(() => {
        const isCorrect: boolean = toKatakana(input) === katakana;
        const isKatakanaNotInitialized = katakana.length !== 0;
        const nextKatakana: string = words[Math.floor(Math.random() * words.length)];

        if (!isKatakanaNotInitialized) {
            console.log("called");
            setKatakana(nextKatakana);
        } else if (isCorrect) {
            const nextCount = count + 1;
            setCount(nextCount);
            setInput("");
            setKatakana(nextKatakana);
        }
    }, [input]);

    // Animate healthbar
    useEffect(() => {
        (async () => {
            let green = "#84cc16"; // lime-500
            let red = "#f43f5e"; // rose-500

            // Reset
            await animate(
                "#lifebar_health",
                { width: "100%", backgroundColor: green },
                { duration: 0.1 },
            );
            // Animate bar down
            await animate(
                "#lifebar_health",
                { width: "0%", backgroundColor: [green, red] },
                { duration: ttl },
            );
        })();
    }, [count]);

    // Set time to live timeout
    useEffect(() => {
        const timeout = setTimeout(() => router.push(`/katakana/${params.level}/gameover?score=${score}`), ttl * 1_000);
        return () => clearTimeout(timeout);
    }, [katakana]);

    useEffect(() => {
        let input = document.querySelector("#input") as HTMLInputElement;
        input?.focus();
    }, []);

    return (
        <div className="flex flex-col gap-4 items-center justify-center w-full h-full">
            <div className="absolute top-1 right-1">
                <Card className="absolute right-4 top-4 p-2 flex gap-2">
                    <span>Score:</span>
                    <strong>{count * 100}</strong>
                </Card>
            </div>

            <Card className="p-4">
                <h1 className="text-center">
                    {preview}
                </h1>
            </Card>

            <div className="w-[300px]">
                <Input
                    id="input"
                    placeholder="go fast!"
                    value={input}
                    onChange={(e) => setInput(e.currentTarget.value)}
                />
            </div>

            <Card id="lifebar" className="w-full p-1">
                <div id="lifebar_health" className="w-full rounded h-[10px] bg-black"></div>
            </Card>
        </div>
    );
}
