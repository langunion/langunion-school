"use client";
import { useState } from "react";
import { Button } from "@/components/ui/button";
import { Card, CardHeader, CardTitle, CardContent, CardFooter } from "@/components/ui/card";
import { ScrollArea } from "@/components/ui/scroll-area";
import { useRouter } from "next/navigation";
import { ArrowLongRightIcon } from "@heroicons/react/24/solid";
import words from "./Katakana.Words";

export default function KatakanaPage() {
    const router = useRouter();
    const [level, setLevel] = useState<string>("");

    return (
        <main className="flex w-full h-full justify-center items-center">
            <Card className="w-[500px]">
                <CardHeader>
                    <CardTitle className="text-3xl text-center">カタカナ Speedrun Game</CardTitle>
                </CardHeader>

                <CardContent>
                    <Card>
                        <CardHeader>
                            <CardTitle>Choose a level:</CardTitle>
                        </CardHeader>
                        <CardContent>
                            <ScrollArea className="h-[200px] w-full">
                                <div className="flex flex-col gap-2">
                                    {Object.keys(words).map(l => (
                                        <Button
                                            key={l}
                                            variant={level === l ? "default" : "outline"}
                                            onClick={() => setLevel(l as any)}
                                        >
                                            <div className="flex gap-4 justify-between items-center w-full">
                                                {l}
                                                <ArrowLongRightIcon className="h-[1rem]" />
                                            </div>
                                        </Button>
                                    ))}
                                </div>
                            </ScrollArea>
                        </CardContent>
                    </Card>
                </CardContent>

                <CardFooter>
                    <Button
                        variant={level ? "default" : "outline"}
                        disabled={!!!level}
                        onClick={() => router.push(`/katakana/${level}`)}
                        className="w-full"
                    >
                        Start
                    </Button>
                </CardFooter>
            </Card>
        </main>
    )
}
