"use client";

import { useState, useMemo } from "react";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import { Card, CardHeader, CardTitle, CardDescription, CardContent, CardFooter } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { MagnifyingGlassIcon, XMarkIcon } from "@heroicons/react/24/solid";
import {
    Tooltip,
    TooltipContent,
    TooltipProvider,
    TooltipTrigger,
} from "@/components/ui/tooltip";
import { ThemeToggle } from "@/components/theme-toggle";

export default function Page() {
    const cards = [
        {
            title: "Katakana Game",
        },
        {
            title: "Japanisch Unterricht",
        },
    ];

    const [search, setSearch] = useState<string>("");

    const filteredCards = useMemo(() => {
        if (!search) return cards;
        return cards.map(card => {
            for (const val of Object.values(card)) {
                if (JSON.stringify(val).toLowerCase().includes(search)) return card;
            }
        }).filter(c => !!c);
    }, [search]);

    return (
        <main className="flex flex-col gap-2 h-full">
            <Card className="flex gap-2 items-center p-2">
                <MagnifyingGlassIcon className="h-[1.5rem] w-[1.5rem]" />
                <Input
                    placeholder="Search ..."
                    value={search}
                    onChange={(e) => setSearch(e.currentTarget.value)}
                />

                <TooltipProvider>
                    <Tooltip>
                        <TooltipTrigger asChild>
                            <Button variant="destructive" onClick={() => setSearch("")}>
                                <XMarkIcon className="h-[1.0rem] w-[1.0rem]" />
                            </Button>
                        </TooltipTrigger>
                        <TooltipContent>
                            <p>Remove search query</p>
                        </TooltipContent>
                    </Tooltip>
                </TooltipProvider>

                <ThemeToggle />
            </Card>

            <Card className="flex gap-2 h-full p-2">
                {filteredCards.map((card, index) => (
                    <Link key={index} href="/katakana">
                        <Card className="h-[200px] w-[200px] flex justify-center overflow-hidden hover:[&>img]:grayscale-0">
                            <h1 className="absolute font-bold text-white z-10">{card.title}</h1>
                            <img src="https://picsum.photos/200" className="grayscale opacity-80 transition" />
                        </Card>
                    </Link>
                ))}
            </Card>
        </main>
    );
}
