const file = Bun.file("./JMdict");
const jmdict = await file.text();

const regex = new RegExp(`<reb>[ァ-ヶー]+</reb>`, "g");

let result = [...jmdict.matchAll(regex)]

const words = result.flat(Infinity).map((word: string) => {
    return word.split(/(<\/?reb>)/)[2]
});

const a = [];
const ka = [];
const sa = [];
const ta = [];
const na = [];
const ha = [];
const ma = [];
const ya = [];
const ra = [];
const wa = [];
const all = [];

words.forEach(word => {
    /* ・ ー ヽ ヾ ヿ
     * ァ ア ィ イ ゥ ウ ェ エ ォ オ
     * カ ガ キ ギ ク グ ケ ゲ コ ゴ ヵ ヶ
     * サ ザ シ ジ ス ズ セ ゼ ソ ゾ
     * タ ダ チ ヂ ッ ツ ヅ テ デ ト ド
     * ナ ニ ヌ ネ ノ
     * ハ バ パ ヒ ビ ピ フ ブ プ ヘ ベ ペ ホ ボ ポ
     * マ ミ ム メ モ
     * ャ ヤ ュ ユ ョ ヨ
     * ラ リ ル レ ロ
     * ヮ ワ ヰ ヱ ヲ ン ヴ ヷ ヸ ヹ ヺ
     */
    const a_row = "ア,イ,ウ,エ,オ";
    const ka_row = "カ,ガ,キ,ギ,ク,グ,ケ,ゲ,コ,ゴ";
    const sa_row = "サ,ザ,シ,ジ,ス,ズ,セ,ゼ,ソ,ゾ";
    const ta_row = "タ,ダ,チ,ヂ,ッ,ツ,ヅ,テ,デ,ト,ド";
    const na_row = "ナ,ニ,ヌ,ネ,ノ";
    const ha_row = "ハ,バ,パ,ヒ,ビ,ピ,フ,ブ,プ,ヘ,ベ,ペ,ホ,ボ,ポ";
    const ma_row = "マ,ミ,ム,メ,モ";
    const ya_row = "ヤ,ユ,ヨ";
    const ra_row = "ラ,リ,ル,レ,ロ";
    const wa_row = "ワ,ヲ,ン";
    const all_row = "ャ,ュ,ョ,ァ,ィ,ゥ,ェ,ォ,ヵ,ヶ,ヮ,ヰ,ヱ,ヴ,ヷ,ヸ,ヹ,ヺ,・,ー,ヽ,ヾ,ヿ";

    const aRegex = new RegExp(`^[${a_row}]+$`);
    if (word.match(aRegex)) a.push(word);

    const kaRegexStr = `${a_row},${ka_row}`
    const kaRegex = new RegExp(`^[${kaRegexStr}]+$`);
    if (word.match(kaRegex)) ka.push(word);

    const saRegexStr = `${kaRegexStr},${sa_row}`
    const saRegex = new RegExp(`^[${saRegexStr}]+$`);
    if (word.match(saRegex)) sa.push(word);

    const taRegexStr = `${saRegexStr},${ta_row}`
    const taRegex = new RegExp(`^[${taRegexStr}]+$`);
    if (word.match(taRegex)) ta.push(word);

    const naRegexStr = `${taRegexStr},${na_row}`
    const naRegex = new RegExp(`^[${naRegexStr}]+$`);
    if (word.match(naRegex)) na.push(word);

    const haRegexStr = `${naRegexStr},${ha_row}`
    const haRegex = new RegExp(`^[${haRegexStr}]+$`);
    if (word.match(haRegex)) ha.push(word);

    const maRegexStr = `${haRegexStr},${ma_row}`
    const maRegex = new RegExp(`^[${maRegexStr}]+$`);
    if (word.match(maRegex)) ma.push(word);

    const yaRegexStr = `${maRegexStr},${ya_row}`
    const yaRegex = new RegExp(`^[${yaRegexStr}]+$`);
    if (word.match(yaRegex)) ya.push(word);

    const raRegexStr = `${yaRegexStr},${ra_row}`
    const raRegex = new RegExp(`^[${raRegexStr}]+$`);
    if (word.match(raRegex)) ra.push(word);

    const waRegexStr = `${raRegexStr},${wa_row}`
    const waRegex = new RegExp(`^[${waRegexStr}]+$`);
    if (word.match(waRegex)) wa.push(word);

    const allRegexStr = `${waRegexStr},${all_row}`
    const allRegex = new RegExp(`^[${allRegexStr}]+$`);
    if (word.match(allRegex)) all.push(word);
    all.push(word)
});

console.log({ all });
console.log(all.length)

/* await Bun.write(
*     "./src/components/Katakana.Words.ts",
*     "export default { \n" +
*     "a: " + "[" + a.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ka: " + "[" + ka.map(w => `"${w}"`).join(", ") + "], \n" +
*     "sa: " + "[" + sa.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ta: " + "[" + ta.map(w => `"${w}"`).join(", ") + "], \n" +
*     "na: " + "[" + na.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ha: " + "[" + ha.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ma: " + "[" + ma.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ya: " + "[" + ya.map(w => `"${w}"`).join(", ") + "], \n" +
*     "ra: " + "[" + ra.map(w => `"${w}"`).join(", ") + "], \n" +
*     "wa: " + "[" + wa.map(w => `"${w}"`).join(", ") + "], \n" +
*     "all: " + "[" + all.map(w => `"${w}"`).join(", ") + "], \n" +
*     "}",
* ); */
